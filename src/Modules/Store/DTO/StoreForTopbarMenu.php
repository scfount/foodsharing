<?php

namespace Foodsharing\Modules\Store\DTO;

class StoreForTopbarMenu
{
	public int $id;
	public string $name;
	public int $pickupStatus;
}
